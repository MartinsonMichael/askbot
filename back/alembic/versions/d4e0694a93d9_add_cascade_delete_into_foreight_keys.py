"""add cascade delete into foreight keys

Revision ID: d4e0694a93d9
Revises: 3dafb54cef6e
Create Date: 2022-01-24 18:25:21.026376

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd4e0694a93d9'
down_revision = '3dafb54cef6e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('uqs_user_id_fkey', 'uqs', type_='foreignkey')
    op.drop_constraint('uqs_schedule_id_fkey', 'uqs', type_='foreignkey')
    op.drop_constraint('uqs_question_id_fkey', 'uqs', type_='foreignkey')
    op.create_foreign_key(None, 'uqs', 'user', ['user_id'], ['user_id'], ondelete='CASCADE')
    op.create_foreign_key(None, 'uqs', 'question', ['question_id'], ['question_id'], ondelete='CASCADE')
    op.create_foreign_key(None, 'uqs', 'schedule', ['schedule_id'], ['schedule_id'], ondelete='CASCADE')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'uqs', type_='foreignkey')
    op.drop_constraint(None, 'uqs', type_='foreignkey')
    op.drop_constraint(None, 'uqs', type_='foreignkey')
    op.create_foreign_key('uqs_question_id_fkey', 'uqs', 'question', ['question_id'], ['question_id'])
    op.create_foreign_key('uqs_schedule_id_fkey', 'uqs', 'schedule', ['schedule_id'], ['schedule_id'])
    op.create_foreign_key('uqs_user_id_fkey', 'uqs', 'user', ['user_id'], ['user_id'])
    # ### end Alembic commands ###

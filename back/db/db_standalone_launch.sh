#!/bin/bash

docker run -it \
  --rm --name db \
  -e POSTGRES_USER=askbot \
  -e POSTGRES_PASSWORD=123 \
  -e POSTGRES_DB=askbot \
  -p 5432:5432 \
  -v askbot_postgres_data_dev:/var/lib/postgresql/data/ \
  postgres:14.1
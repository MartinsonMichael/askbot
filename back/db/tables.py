import enum
import datetime

import sqlalchemy as db
import sqlalchemy_utils as db_utils
from sqlalchemy.dialects.postgresql import ARRAY

from db.utils import DB_URL

Engine = db.create_engine(DB_URL)
_meta_data = db.MetaData(Engine)


UserDB = db.Table(
    'user', _meta_data,
    db.Column('user_id', db.Integer, primary_key=True, autoincrement=True),

    db.Column('telegram_chat_id', db.Integer, nullable=True),
    db.Column('name', db.String(128), default=""),

    db.Column('time_zone', db_utils.TimezoneType, default="")
)


UQS_DB = db.Table(
    'uqs', _meta_data,
    db.Column('uqs_id', db.Integer, primary_key=True, autoincrement=True),
    db.Column('user_id', db.Integer, db.ForeignKey('user.user_id', ondelete="CASCADE"), nullable=False),
    db.Column('question_id', db.Integer, db.ForeignKey('question.question_id', ondelete="CASCADE"), nullable=False),
    db.Column('schedule_id', db.Integer, db.ForeignKey('schedule.schedule_id', ondelete="CASCADE"), nullable=False),
)


QuestionDB = db.Table(
    'question', _meta_data,
    db.Column('question_id', db.Integer, primary_key=True, autoincrement=True),

    db.Column('text', db.String(4000), default=""),
    db.Column('answer_min_value', db.Integer, default=0),
    db.Column('answer_max_value', db.Integer, default=2),
    db.Column('answer_step', db.Integer, default=1)
)


ResultsDB = db.Table(
    'results', _meta_data,
    db.Column('result_id', db.Integer, primary_key=True, autoincrement=True),
    db.Column('question_id', db.Integer, db.ForeignKey('question.question_id', ondelete="CASCADE"), nullable=False),
    db.Column('user_id', db.Integer, db.ForeignKey('user.user_id', ondelete="CASCADE"), nullable=False),

    db.Column('dttm_submitted', db.DateTime, default=datetime.datetime.now),
    db.Column('dttm_answer', db.DateTime, nullable=True),

    db.Column('result', db.Integer, nullable=True),
)


class TimeIntervals(enum.Enum):
    Day = 1
    Week = 2


class ScheduleState(enum.Enum):
    Running = 1
    Paused = 2


ScheduleDB = db.Table(
    'schedule', _meta_data,
    db.Column('schedule_id', db.Integer, primary_key=True, autoincrement=True),

    db.Column('dttm_start', db.DateTime, default=datetime.datetime.now),
    db.Column(
        'state',
        db_utils.ChoiceType(ScheduleState, impl=db.Integer()),
        nullable=False,
        default=ScheduleState.Running,
    ),

    # db.Column('repeat_every_value', db.Integer, default=1),
    # db.Column('repeat_every_units', db_utils.ChoiceType(TimeIntervals, impl=db.Integer()), default=TimeIntervals.Day),

    db.Column('time_list', ARRAY(db.Time), default=[datetime.time(hour=12)], nullable=False),
)

_meta_data.create_all(Engine)

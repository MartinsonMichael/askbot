import sqlalchemy as db
from db.tables import Engine


def test_db_connection():
    for table_name in ['schedule', 'question', 'user', 'results', 'uqs']:
        inspect_obj = db.inspect(subject=Engine)

        assert table_name in inspect_obj.get_table_names(), f"{table_name} should exists"

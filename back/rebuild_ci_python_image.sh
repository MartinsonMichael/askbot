#!/bin/bash

echo "Freezeing pip requirements into requirements.txt..."
pip freeze > requirements.txt

echo "Rebuilding image..."
docker build -f Dockerfile_CI -t registry.gitlab.com/martinsonmichael/askbot/python-test .

echo "Pushing image into the gitlab registry..."
docker push registry.gitlab.com/martinsonmichael/askbot/python-test
#!/bin/bash

docker run -it \
  --rm --name rabbit-ask \
  -p 5672:5672 \
  -p 15672:15672 \
  -e RABBITMQ_DEFAULT_USER=askbot \
  -e RABBITMQ_DEFAULT_PASS=123 \
  rabbitmq:3.9-management

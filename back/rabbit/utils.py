import os
import pika

env = os.environ

# amqp://username:password@host:port/<virtual_host>[?query-string]
RabbitConnection = pika.connection.URLParameters(
    f"amqp://{env['RABBIT_USER']}:{env['RABBIT_PASSWORD']}@{env['RABBIT_HOST']}:{env['RABBIT_PORT']}/"
)

import os
import json
import logging
from logging import Logger
from typing import Dict

import pika
from freezegun import freeze_time
from pika.adapters.blocking_connection import BlockingChannel
from pika.spec import Basic, BasicProperties
from sqlalchemy.engine import LegacyCursorResult
from telegram import Bot, ReplyMarkup, Message
from telegram.ext import Updater

import db.tables as tables

from sqlalchemy import delete, insert, select

from rabbit.utils import RabbitConnection
from scheduler.utils import get_bot_master_queue_name, get_queue_name_for_user
from cache.utils import redis_client


logger = logging.getLogger(__name__)
env = os.environ

updater = Updater(token=env['BOT_TOKEN'], use_context=True)
bot: Bot = updater.bot


# def _scheduler_event_callback(
#         channel: BlockingChannel, method: Basic.Deliver, properties: BasicProperties, body: bytes, arguments: Dict,
# ) -> None:
#     data = json.loads(body.decode())
#     telegram_chat_id = arguments['telegram_chat_id']
#     channel.basic_nack(method.delivery_tag, multiple=False, requeue=True)


def _is_user_ready_for_new_message(user_id: int) -> bool:
    # first check in redis
    if redis_client.get(f"user-busy:{user_id}") is not None:
        return False
    if redis_client.get(f"user-free:{user_id}") is not None:
        return True

    # TODO @MichaelMD
    return True

    # # now check db
    # connection = tables.Engine.connect()
    # result: LegacyCursorResult = connection.execute(
    #     select(tables.ResultsDB.c.user_id, tables.ResultsDB.c.dttm_answer, tables.ResultsDB.c.result)
    #     .where(tables.ResultsDB.c.user_id == user_id)
    #     .sort_by(tables.ResultsDB.c.dttm_submitted.desc())
    #     .limit(1)
    # )
    # user_id, dttm_answer, result = result.first()


def _send_next_message(user_id: int) -> None:
    rabbit_connection = pika.BlockingConnection(RabbitConnection)
    rabbit_channel = rabbit_connection.channel()
    rabbit_channel.queue_declare(queue=get_queue_name_for_user(user_id))
    method, properties, body = rabbit_channel.basic_get(queue=get_queue_name_for_user(user_id))
    if method is None and properties is None and body is None:
        logger.warning(f"NO MESSAGE ON ACTION, `message_ready`, queue name: {get_queue_name_for_user(user_id)}")
        return
    assert isinstance(body, str), f"body was declared to be str, but it {type(body)}"
    data = json.loads(body)
    msg: Message = bot.send_message(
        chat_id=data['telegram_chat_id'],
        text=data['text'],
        reply_markup=[[
            {"text": x, "callback_data": x} for x in data['answer_range']
        ]],
    )
    print(msg)


def _schedule_master_events(
    channel: BlockingChannel, method: Basic.Deliver, properties: BasicProperties, body: bytes, arguments: Dict,
) -> None:
    data = json.loads(bytes.decode(body))
    assert isinstance(data, dict)
    assert 'action' in data.keys()

    if data['action'] == 'message_ready':
        user_id = int(data['user_id'])
        if _is_user_ready_for_new_message(user_id=user_id):
            _send_next_message(user_id=user_id)


def setup():
    logger.debug("Start to setup TelegramBot")
    rabbit_connection = pika.BlockingConnection(RabbitConnection)
    rabbit_channel = rabbit_connection.channel()
    rabbit_channel.queue_declare(queue=get_bot_master_queue_name())
    rabbit_channel.basic_consume(
        queue=get_bot_master_queue_name(),
        on_message_callback=_schedule_master_events,
        auto_ack=False,
    )
    logger.debug(f"Add event master callback")

    rabbit_channel.start_consuming()
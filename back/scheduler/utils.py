from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.executors.pool import ThreadPoolExecutor

from datetime import datetime, timedelta, time, date
from typing import Tuple, Optional
from pytz import utc

from db.utils import DB_URL
import db.tables as tables


jobstores = {
    'default': SQLAlchemyJobStore(url=DB_URL)
}
executors = {
    'default': ThreadPoolExecutor(4),
}
job_defaults = {
    'coalesce': False,
    'max_instances': 3
}
Scheduler = BackgroundScheduler(
    jobstores=jobstores,
    executors=executors,
    job_defaults=job_defaults,
    timezone=utc,
)


def _time_to_timedelta(tm: time) -> timedelta:
    return timedelta(hours=tm.hour, minutes=tm.minute, seconds=tm.second)


def _day_plus_time(dt: date, tm: time) -> datetime:
    return datetime(year=dt.year, month=dt.month, day=dt.day, hour=tm.hour, minute=tm.minute, second=tm.second)


def get_nearest_execution_dttm(schedule: Tuple) -> Optional[datetime]:
    assert isinstance(schedule, (tuple, list)), f"`schedule` should be list of tuple, got `{type(schedule)}`"
    schedule_id, time_list, state, dttm_start = schedule
    assert isinstance(schedule_id, int), f"`schedule_id` should be int, got `{type(schedule_id)}`"
    assert isinstance(state, tables.ScheduleState), f"`state` should be ScheduleState, got `{type(state)}`"
    assert isinstance(dttm_start, datetime), f"`dttm_start` should be datetime, got `{type(dttm_start)}`"
    assert isinstance(time_list, list), f"`time_list` should be list, got `{type(time_list)}`"
    assert sorted(time_list) == time_list, f"`time_list` should be sorted, got {time_list}"

    if state == tables.ScheduleState.Paused:
        return None

    now_time = datetime.now().time()

    for exec_time in time_list:
        if exec_time > now_time:
            return _day_plus_time(datetime.now().date(), exec_time)

    # return first time in a list for next day
    return _day_plus_time(datetime.now().date() + timedelta(days=1), time_list[0])


def get_queue_name_for_user(user_id: int) -> str:
    return f"chat-id:{user_id}"


def get_bot_master_queue_name() -> str:
    return "bot-events-master"

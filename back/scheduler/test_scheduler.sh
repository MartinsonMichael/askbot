#!/bin/bash

# db credentials
export DB_HOST=localhost
export DB_PORT=5432
export DB_USER=askbot
export DB_NAME=askbot
export DB_PASSWORD=123

# rabbit mq credentials
export RABBIT_USER=askbot
export RABBIT_PASSWORD=123
export RABBIT_HOST=localhost
export RABBIT_PORT=5672

pytest --capture=tee-sys


import json

import pika
from freezegun import freeze_time
from pika.adapters.blocking_connection import BlockingChannel
from pika.spec import Basic, BasicProperties
from sqlalchemy.engine import LegacyCursorResult
from sqlalchemy.engine.mock import MockConnection
from datetime import datetime, time, tzinfo, timezone, timedelta
from dateutil import tz

import db.tables as tables

from sqlalchemy import delete, insert, select

from rabbit.utils import RabbitConnection
from scheduler.routine import _check, _job_func
from scheduler.utils import Scheduler


def _clear_test_db(connection: MockConnection) -> None:
    connection.execute(delete(tables.UserDB))
    connection.execute(delete(tables.ScheduleDB))
    connection.execute(delete(tables.QuestionDB))
    connection.execute(delete(tables.ResultsDB))
    connection.execute(delete(tables.UQS_DB))


def _refill_test_db(connection: MockConnection):
    _clear_test_db(connection)
    connection.execute(
        insert(tables.UserDB).values([
            {"user_id": 0, "telegram_chat_id": 0, "name": "Michael", "time_zone": "Europe/Zurich"},
            {"user_id": 1, "telegram_chat_id": 1, "name": "Jenia", "time_zone": "Europe/Zurich"},
        ])
    )
    connection.execute(
        insert(tables.QuestionDB).values([
            {"question_id": 0, "text": "Do you like weather?", "answer_min_value": 0, "answer_max_value": 2, "answer_step": 1},
            {"question_id": 1, "text": "WTF?", "answer_min_value": 0, "answer_max_value": 2, "answer_step": 1},
        ])
    )
    connection.execute(
        insert(tables.ScheduleDB).values(
            {
                "schedule_id": 0,
                "dttm_start": datetime(year=2022, month=1, day=20),
                "state": tables.ScheduleState.Running,
                "time_list": [time(hour=14, minute=30)],
            },
        )
    )
    connection.execute(
        insert(tables.UQS_DB).values([
            {"uqs_id": 0, "user_id": 0, "question_id": 0, "schedule_id": 0},
            {"uqs_id": 1, "user_id": 1, "question_id": 1, "schedule_id": 0},
        ])
    )


@freeze_time(datetime(year=2022, month=1, day=24))
def test__initial_adding__check():
    connection = tables.Engine.connect()

    _refill_test_db(connection)

    assert [res for res in connection.execute(select(tables.UserDB))].__len__() == 2
    assert [res for res in connection.execute(select(tables.QuestionDB))].__len__() == 2
    assert [res for res in connection.execute(select(tables.ScheduleDB))].__len__() == 1
    assert [res for res in connection.execute(select(tables.UQS_DB))].__len__() == 2

    # prepare Scheduler
    Scheduler.remove_all_jobs()

    _check()

    job_list = Scheduler.get_jobs()
    assert job_list.__len__() == 2

    UTC_TIME_ZONE = timezone(timedelta())
    for job in job_list:
        assert job.next_run_time == datetime(year=2022, month=1, day=24, hour=14, minute=30, tzinfo=UTC_TIME_ZONE)

    # after test clearing
    Scheduler.remove_all_jobs()
    _clear_test_db(connection)


def _rabbit_callback_like_telegram_bot(channel: BlockingChannel, method: Basic.Deliver, properties: BasicProperties, body: bytes) -> None:
    data = json.loads(body.decode())

    assert isinstance(data, dict)
    for key in ['telegram_chat_id', 'result_id', 'text', 'answer_range']:
        assert key in data.keys()
    assert data['telegram_chat_id'] == 0
    assert data['text'] == "Do you like weather?"
    assert data['answer_range'] == [0, 1, 2]

    connection = tables.Engine.connect()
    result: LegacyCursorResult = connection.execute(
        select(
            tables.ResultsDB.c.user_id, tables.ResultsDB.c.question_id, tables.ResultsDB.c.dttm_submitted
        ).where(
            tables.ResultsDB.c.result_id == data['result_id']
        )
    )
    user_id, question_id, dttm_submitted = result.first()
    # assert dttm_submitted == datetime.now()
    assert user_id == 0
    assert question_id == 0

    channel.basic_ack(delivery_tag=method.delivery_tag)
    channel.stop_consuming()


@freeze_time(datetime(year=2022, month=1, day=24, hour=14, minute=24))
def test__rabbit__job_func():
    connection = tables.Engine.connect()

    _refill_test_db(connection)

    rabbit_connection = pika.BlockingConnection(RabbitConnection)
    rabbit_channel = rabbit_connection.channel()
    queue_name = f"chat-id:{0}"
    rabbit_channel.queue_delete(queue=queue_name)

    # prepare Scheduler
    Scheduler.remove_all_jobs()

    assert [res for res in connection.execute(select(tables.UserDB))].__len__() == 2
    assert [res for res in connection.execute(select(tables.QuestionDB))].__len__() == 2
    assert [res for res in connection.execute(select(tables.ScheduleDB))].__len__() == 1
    assert [res for res in connection.execute(select(tables.UQS_DB))].__len__() == 2

    _job_func(0)

    rabbit_channel.basic_consume(
        queue=queue_name,
        on_message_callback=_rabbit_callback_like_telegram_bot,
        auto_ack=False,
    )

    rabbit_channel.start_consuming()

    res = rabbit_channel.queue_declare(queue=queue_name)
    assert res.method.message_count == 0

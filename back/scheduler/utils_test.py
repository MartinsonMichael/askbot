import db.tables as tables
from datetime import datetime
from freezegun import freeze_time

from scheduler.utils import get_nearest_execution_dttm


@freeze_time(datetime(year=2022, month=1, day=22))
def test__first_time__get_nearest_execution_dttm():
    # schedule_id, time_list, state, dttm_start = schedule
    TEST_DTTM_1 = datetime(year=2022, month=1, day=22, hour=14, minute=18, second=12)
    TEST_DTTM_2 = datetime(year=2022, month=1, day=22, hour=17, minute=20, second=0)
    assert TEST_DTTM_1 == get_nearest_execution_dttm((
        1,
        [TEST_DTTM_1.time(), TEST_DTTM_2.time()],
        tables.ScheduleState.Running,
        datetime(year=2022, month=1, day=1),
    ))


@freeze_time(datetime(year=2022, month=1, day=21, hour=19, minute=3))
def test__next_day__get_nearest_execution_dttm():
    # schedule_id, time_list, state, dttm_start = schedule
    TEST_DTTM_1 = datetime(year=2022, month=1, day=22, hour=14, minute=18, second=12)
    TEST_DTTM_2 = datetime(year=2022, month=1, day=22, hour=17, minute=20, second=0)
    assert TEST_DTTM_1 == get_nearest_execution_dttm((
        1,
        [TEST_DTTM_1.time(), TEST_DTTM_2.time()],
        tables.ScheduleState.Running,
        datetime(year=2022, month=1, day=1),
    ))


@freeze_time(datetime(year=2022, month=1, day=22, hour=15, minute=3))
def test__second_time__get_nearest_execution_dttm():
    # schedule_id, time_list, state, dttm_start = schedule
    TEST_DTTM_1 = datetime(year=2022, month=1, day=22, hour=14, minute=18, second=12)
    TEST_DTTM_2 = datetime(year=2022, month=1, day=22, hour=17, minute=20, second=0)
    assert TEST_DTTM_2 == get_nearest_execution_dttm((
        1,
        [TEST_DTTM_1.time(), TEST_DTTM_2.time()],
        tables.ScheduleState.Running,
        datetime(year=2022, month=1, day=1),
    ))

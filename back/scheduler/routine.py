import json
from typing import List, Tuple

from apscheduler.job import Job
import pika
from sqlalchemy.engine import LegacyCursorResult

from db.tables import Engine, ScheduleDB, QuestionDB, UQS_DB, UserDB, ResultsDB
from sqlalchemy.sql import select, insert

from scheduler.utils import Scheduler
import scheduler.utils as utils
from rabbit.utils import RabbitConnection


def start_scheduler() -> None:
    _check()
    Scheduler.start()


def _check():
    job_list: List[Job] = Scheduler.get_jobs()

    connection = Engine.connect()
    for result in connection.execute(
            select(
                UQS_DB.c.uqs_id, ScheduleDB.c.schedule_id, ScheduleDB.c.time_list, ScheduleDB.c.state, ScheduleDB.c.dttm_start
            ).where(
                ScheduleDB.c.schedule_id == UQS_DB.c.schedule_id
            )
    ):
        uqs_id, *schedule = result
        next_run_dttm = utils.get_nearest_execution_dttm(schedule)
        for job in job_list:
            # schedule[0] is a schedule_id
            if job.name == str(uqs_id) and job.next_run_time == next_run_dttm:
                break
        # else - case if not a break
        else:
            add_job(schedule, uqs_id)


def add_job(schedule: Tuple, uqs_id: int) -> None:
    next_run_dttm = utils.get_nearest_execution_dttm(schedule)
    Scheduler.add_job(
        func=_job_func,
        kwargs={
            'uqs_id': uqs_id,
        },
        id=str(uqs_id),
        next_run_time=next_run_dttm,
    )


def _job_func(uqs_id: int) -> None:
    connection = Engine.connect()

    result: LegacyCursorResult = connection.execute(
        select(
            UQS_DB.c.uqs_id,
            ScheduleDB.c.schedule_id, ScheduleDB.c.time_list, ScheduleDB.c.state, ScheduleDB.c.dttm_start,
            QuestionDB.c.question_id, QuestionDB.c.text, QuestionDB.c.answer_min_value, QuestionDB.c.answer_max_value, QuestionDB.c.answer_step,
            UserDB.c.user_id, UserDB.c.telegram_chat_id,
        )
        .where(
            UQS_DB.c.uqs_id == uqs_id,
            UQS_DB.c.schedule_id == ScheduleDB.c.schedule_id,
            UQS_DB.c.user_id == UserDB.c.user_id,
            UQS_DB.c.question_id == QuestionDB.c.question_id,
        )
        .limit(1)
    )
    uqs_id, schedule_id, time_list, state, dttm_start, question_id, text, answer_min_value, answer_max_value, answer_step, user_id, telegram_chat_id = result.first()

    result_id: int = connection.execute(
        insert(ResultsDB).values({"question_id": question_id, "user_id": user_id})
    ).inserted_primary_key[0]

    rabbit_connection = pika.BlockingConnection(RabbitConnection)
    queue_name = utils.get_queue_name_for_user(user_id)
    rabbit_channel = rabbit_connection.channel()
    rabbit_channel.queue_declare(queue=queue_name)

    rabbit_channel.basic_publish(
        exchange='',
        routing_key=queue_name,
        body=json.dumps({
            "telegram_chat_id": telegram_chat_id,
            "result_id": result_id,
            "text": text,
            "answer_range": list(range(answer_min_value, answer_max_value + answer_step, answer_step)),
        }),
    )

    rabbit_channel.queue_declare(queue=utils.get_bot_master_queue_name())
    rabbit_channel.basic_publish(
        exchange='',
        routing_key=utils.get_bot_master_queue_name(),
        body=json.dumps({"action": "message_ready", "user_id": user_id}),
    )


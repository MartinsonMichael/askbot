#!/bin/bash

if [[ "$#" != 1 ]]; then
  echo "You need exactly one argument - db shema update message"
  exit 1
fi;

export DB_HOST=localhost
export DB_PORT=5432
export DB_USER=askbot
export DB_NAME=askbot
export DB_PASSWORD=123

alembic revision --autogenerate -m "$1"

alembic upgrade head
import logging
from logging import Logger

from telegram import Update
from telegram.ext import CallbackContext

logger: Logger = logging.getLogger(__name__)


def process_request(update: Update, context: CallbackContext) -> None:
    logger.debug(f"MSG ({update.effective_chat.username}) -> `{update}`")

    context.dispatcher.bot.send_message(chat_id=update.effective_chat.id, text="Got it! Will process later...")

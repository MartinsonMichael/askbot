#!/bin/bash

echo "Start this script on server to test TelegramBot webhooks!"

docker run -it \
  --rm --name askbot-webhooks-test \
  -e BOT_TOKEN="5225144832:AAG-SMcApNRq-YT6ws4J8nY1jerfkiikcys" \
  -e BOT_PORT=8443 \
  -e SSL_CERT_PRIVATE_KEY_PATH="/home/michael/private.key" \
  -e SSL_CERT_CERT_PEM_PATH="/home/michael/cert.pem" \
  -e DOMAIN="62.84.125.200" \
  -v $PWD/..:/code \
  -v /home/michael/private.key:/home/michael/private.key \
  -v /home/michael/cert.pem:/home/michael/cert.pem \
  -p 8443:8443 \
  registry.gitlab.com/martinsonmichael/askbot/python-test bash
#  ls -lah code/webhook && cat code/webhook/webhook.py && python code/webhook/webhook.py

import os
import logging
from logging import Logger

from telegram import Bot, WebhookInfo
from telegram.ext import Updater, Dispatcher, MessageHandler, Filters

from webhook.webhook_processing import process_request

logger: Logger = logging.getLogger(__name__)

env = os.environ
WEBHOOK_URL = f"https://{env['DOMAIN']}:{env['BOT_PORT']}/{env['BOT_TOKEN']}"

logger.debug(f"Webhook url to use: `{WEBHOOK_URL}`")

updater = Updater(token=env['BOT_TOKEN'], use_context=True)
bot: Bot = updater.bot

webhook_info: WebhookInfo = bot.get_webhook_info()
logger.debug(f"Existing webhooks info:\ninfo: {webhook_info}\nurl: {webhook_info.url}")

if webhook_info.url != WEBHOOK_URL:
    logger.debug(f"Existing webhook address is different, so setting new one.")
    webhooks_setup = bot.set_webhook(
        url=WEBHOOK_URL,
        certificate=open(env['SSL_CERT_CERT_PEM_PATH'], 'rb'),
    )
    logger.debug(f"Webhooks setup status: {webhooks_setup}")
    webhook_info: WebhookInfo = bot.get_webhook_info()
    logger.debug(f"Updated webhooks info:\ninfo: {webhook_info}\nurl: {webhook_info.url}")


dispatcher: Dispatcher = updater.dispatcher
dispatcher.add_handler(
    MessageHandler(filters=Filters.all, callback=process_request)
)

logger.debug("Starting listening for webhooks...")
updater.start_webhook(
    listen='0.0.0.0',
    port=env['BOT_PORT'],
    url_path=env['BOT_TOKEN'],
    key=env['SSL_CERT_PRIVATE_KEY_PATH'],
    cert=env['SSL_CERT_CERT_PEM_PATH'],
    webhook_url=WEBHOOK_URL,
)

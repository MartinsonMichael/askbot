#!/bin/bash

export REDIS_HOST=localhost
export REDIS_PORT=6379
export REDIS_USER=askbot
export REDIS_PASSWORD=123

pytest --capture=tee-sys

#!/bin/bash

docker run -it \
  --rm --name redis-ask \
  -p 6379:6379 \
  -v ${PWD}/redis.conf:/usr/local/etc/redis/redis.conf \
  redis:6.2.6-alpine redis-server /usr/local/etc/redis/redis.conf

from cache.utils import redis_client


def test__redis_connection():
    for key in redis_client.scan_iter("*"):
        redis_client.delete(key)

    redis_client.set("test", "17")
    assert bytes.decode(redis_client.get("test")) == "17"

    redis_client.set("test", "wowo!")
    assert bytes.decode(redis_client.get("test")) == "wowo!"

    assert redis_client.get("test-bool") is None

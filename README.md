# AskBot

AskBot is an application that helps you to track routine stuff comfortably.

The main use case of AskBot is a mood tracking.
Setup questions (or use predefined ones) -> setup preferable schedule ->
bot will as you via messenger -> answer each question in a simple form (just number 0 - 1 - 2) ->
see your mood statistic

## Architecture

<img src="/readme-media/AskBot_architecture.drawio.png"/>

## Used Technology

### Deployment & Monitoring

* __CI__: gitlab ci/cd, tests via pytest
* __CD__: gitlab ci/cd, Docker-Compose
* __monitoring__: Grafana, Prometheus [_in progress_]

### Backend
 
* __languages__: Python, Bash
* __data base__: PostgreSQL in Docker, psycorg2 as driver, SQLAlchemy for tables description, alembic for migrations
* __message broker__: RabbitMQ in Docker, pika
* __events__: Apache Kafka [_in progress_]
* __cache__: Redis
* __scheduler__: APScheduler library as cron wrapper
* __telegram bot__: webhooks 
* __API__: protobuf api description, gRPC [_in progress_]

### Frontend [_in progress_]

* __languages__: Typescript
* __framework__: React.JS, Redux (state storage), Router
* __communication__: gRPC client
* __authorisation__: JWT